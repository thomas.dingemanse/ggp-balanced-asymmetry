# Balanced asymmetry in general game playing

## About this repository

This repository contains the source code for my bachelor's thesis _Balanced asymmetry in general game playing_ at Utrecht University, 2020. This includes the GDL game description for 13x13 historical hnefatafl with the Parlett layout, as well as game descriptions for all variant problems that were mentioned in the thesis. All of the game descriptions are provided in both prefix GDL (`.hrf`) and infix GDL (`.kif`) format. The prefix GDL game descriptions include comments and the infix files do not, but the two are otherwise identical. Some metadata files that were required or just useful in order to run matches of the game in ggp-base are also included: a METADATA file, a plain text description of the game, a thumbnail icon, and a visual representation of the game in Javascript and XSL. Finally, a Pygments GDL lexer supporting both prefix and infix notation is provided to add syntax highlighting to the code in environments where this is supported (such as Overleaf).

The thesis is available at the [Utrecht University Thesis Archive](https://studenttheses.library.uu.nl/search.php?language=en) in `.pdf` format.

## Usage

The experiments mentioned in the thesis can be replicated by following these instructions:
1. Install [ggp-base](https://github.com/ggp-org/ggp-base) and all of its requirements, such as the Java Development Kit 7 or 8. Make sure to read the instructions carefully, as they differ by platform and mistakes could cause problems in subsequent steps.
2. Check if the installation was successful by running (for instance) a local match of tic-tac-toe between two random players using the server interface and the player interface. These can be started separately from the command line.
3. Download the [Customizable General Game Player](http://www.ggp.org/cs227b/player.html) by [GGP.org](http://www.ggp.org/) and execute it.
4. Make two Random players in the Customizable General Game Player and repeat step 2, but now with these two players instead of using the ggp-base player interface.
5. Create a new directory: `ggp-base/games/games/hnefatafl`.
6. Copy all files from this repository to the newly created directory.
7. Restart the server interface if you haven't already and start a local match of 13x13 historical hnefatafl (Parlett layout) with any two players.

To start a match for any of the variant problems, modify the METADATA file by changing the value of `"rulesheet"` to the file you want to use (make sure to use the `.kif` file, `.hrf` doesn't work) and optionally change the value of `"gameName"` to the name of the variant problem for clarity (this is how the game will be displayed in the dropdown menu). The rest of the METADATA file should remain unchanged. Restart the server interface and start a new match of your selected variant problem.

Normally, all GDL code sent to the player is scrambled with a dictionary to make sure all players rely solely on the logical semantics of the game description instead of the linguistic semantics. In this project, none of the agents is able to perform any kind of linguistic analysis, so the setting can safely be disabled by deselecting **Scramble GDL**, which makes it easier to see which moves are being played.

Also note that a number of matches can be run sequentially by selecting **Queue matches?** and selecting a number greater than 1.