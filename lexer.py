# Custom Pygments lexer for Game Description Language (GDL).
# by Thomas Dingemanse.

from pygments.lexer import RegexLexer
from pygments.token import Whitespace, Comment, Operator, Keyword, Name, Number, Punctuation

class PrefixGDLLexer(RegexLexer):
    name = 'Prefix GDL'
    aliases = ['prefix-gdl']
    filenames = ['*.kif']

    tokens = {
        'root': [
            (r'\s+', Whitespace),
            # Comments start with a ; (semicolon) sign and continue to the end of the line.
            (r';.*?\n', Comment),
            # Logical operators.
            (r'\b(or|and|not|distinct)\b', Operator.Word),
            # The rule operator ("follows from").
            (r'<=', Operator),
            # Game-independent GDL keywords.
            (r'\b(role|base|input|init|true|does|next|legal|goal|terminal|noop)\b', Keyword),
            # Variables start with a ? (question mark) sign.
            (r'\?\b\w+\b', Name.Variable),
            # Parentheses.
            (r'[()]', Punctuation),
            # Numeric values such as indices and utility values.
            (r'\b[0-9]+\b', Number),
            # Normal text (non-highlighted).
            (r'\b\w+\b', Name.Label)
        ]
    }

class InfixGDLLexer(RegexLexer):
    name = 'Infix GDL'
    aliases = ['infix-gdl']
    filenames = ['*.hrf']

    tokens = {
        'root': [
            # Spaces, tabs, and newlines have no functional effect in GDL.
            (r'\s+', Whitespace),
            # Comments start with a % (percentage) sign and continue to the end of the line.
            (r'%.*?\n', Comment),
            # Logical operators like & (and), | (or), :- (rule/follows from), and distinct(X,Y).
            (r'(~|\||&|:-)', Operator),
            (r'\bdistinct\b', Operator.Word),
            # Game-independent GDL keywords.
            (r'\b(role|base|input|init|true|does|next|legal|goal|terminal|noop)\b', Keyword),
            # Variables start with a capital letter and are followed by letters, numbers or underscores.
            (r'\b[A-Z]\w*\b', Name.Variable),
            # The _ variable is the unique "don't care" variable.
            (r'\b_\b', Name.Constant),
            # Punctuation symbols that aren't operators.
            (r'[(),]', Punctuation),
            # Numeric values such as indices and utility values.
            (r'\b[0-9]+\b', Number),
            # Custom predicates and terms (non-highlighted).
            (r'\b\w+\b', Name.Label)
        ]
    }

__all__ = ['PrefixGDLLexer', 'InfixGDLLexer']