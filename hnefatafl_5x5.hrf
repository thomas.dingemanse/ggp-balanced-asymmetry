% 13x13 historical Hnefatafl with the Parlett layout.
%   (without restricted corner squares)

% Rules adapted from:
%   <http://aagenielsen.dk/historical_hnefatafl_rules.php>

% Game description by:
%   Thomas Dingemanse <t.dingemanse@students.uu.nl>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%  ROLES  %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Two asymmetric player roles, each with their own goals and initial position.
role(attacker)
role(defender)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%  BASE PROPOSITIONS  %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Non-throne cells have x- and y-coordinates and a value.
base(cell(X, Y, V)) :-
    ~throne(X, Y) &
    index(X) &
    index(Y) &
    value(V)

% The throne can be unoccupied or it can be occupied by the king.
base(cell(3, 3, king))
base(cell(3, 3, blank))

% It's always the turn of either one of the players.
base(control(R)) :- role(R)

% State counts always have a valid game state and a count of 1, 2, or 3.
base(state_count(N,
    X1Y1, X1Y2, X1Y3, X1Y4, X1Y5,
    X2Y1, X2Y2, X2Y3, X2Y4, X2Y5,
    X3Y1, X3Y2, X3Y3, X3Y4, X3Y5,
    X4Y1, X4Y2, X4Y3, X4Y4, X4Y5,
    X5Y1, X5Y2, X5Y3, X5Y4, X5Y5)) :-
    valid_state_count(N) &
    value(X1Y1) &
    value(X1Y2) &
    value(X1Y3) &
    value(X1Y4) &
    value(X1Y5) &
    value(X2Y1) &
    value(X2Y2) &
    value(X2Y3) &
    value(X2Y4) &
    value(X2Y5) &
    value(X3Y1) &
    value(X3Y2) &
    value(X3Y3) &
    value(X3Y4) &
    value(X3Y5) &
    value(X4Y1) &
    value(X4Y2) &
    value(X4Y3) &
    value(X4Y4) &
    value(X4Y5) &
    value(X5Y1) &
    value(X5Y2) &
    value(X5Y3) &
    value(X5Y4) &
    value(X5Y5)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%  FEASIBLE ACTIONS  %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Move vertically to another cell.
input(R, move(X, Y_current, X, Y_goal)) :- 
    role(R) &
    index(X) &
    index(Y_current) &
    index(Y_goal) &
    distinct(Y_current, Y_goal)

% Move horizontally to another cell.
input(R, move(X_current, Y, X_goal, Y)) :-
    role(R) &
    index(X_current) &
    index(X_goal) &
    index(Y) &
    distinct(X_current, X_goal)

% Do nothing.
input(R, noop) :- role(R)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%  INITIAL STATE  %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The attacker has 8 pieces at all four edges of the board, 32 total.
% The defender has a king that is placed on the throne at (7,7).
% There are 16 defender pieces, positioned around the king to defend it.
% Which coordinate represents the horizontal or the vertical direction on the
% board is irrelevant, because it is both horizontally and vertically
% mirror symmetric.

init(cell(1, 1, blank))
init(cell(1, 2, blank))
init(cell(1, 3, attacker))
init(cell(1, 4, blank))
init(cell(1, 5, blank))

init(cell(2, 1, blank))
init(cell(2, 2, blank))
init(cell(2, 3, defender))
init(cell(2, 4, blank))
init(cell(2, 5, blank))

init(cell(3, 1, attacker))
init(cell(3, 2, blank))
init(cell(3, 3, king))
init(cell(3, 4, blank))
init(cell(3, 5, attacker))

init(cell(4, 1, blank))
init(cell(4, 2, blank))
init(cell(4, 3, defender))
init(cell(4, 4, blank))
init(cell(4, 5, blank))

init(cell(5, 1, blank))
init(cell(5, 2, blank))
init(cell(5, 3, attacker))
init(cell(5, 4, blank))
init(cell(5, 5, blank))

% The attacker begins.
init(control(attacker))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%  LEGALITY  %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Both players can move one of their own regular pieces vertically to another cell.
legal_move(R, move(X, Y_current, X, Y_goal)) :- 
    true(control(R)) &
    ~throne(X, Y_goal) &
    true(cell(X, Y_current, R)) &
    succ(Y_current, Y_next) &
    path(X, Y_next, X, Y_goal)

% Descending paths are checked by using the predicate in reverse order.
legal_move(R, move(X, Y_current, X, Y_goal)) :-
    true(control(R)) &
    ~throne(X, Y_goal) &
    true(cell(X, Y_current, R)) &
    succ(Y_prev, Y_current) &
    path(X, Y_goal, X, Y_prev)

% Both players can move one of their own regular pieces horizontally to another cell.
legal_move(R, move(X_current, Y, X_goal, Y)) :-
    true(control(R)) &
    ~throne(X_goal, Y) &
    true(cell(X_current, Y, R)) &
    succ(X_current, X_next) &
    path(X_next, Y, X_goal, Y)

legal_move(R, move(X_current, Y, X_goal, Y)) :-
    true(control(R)) &
    ~throne(X_goal, Y) &
    true(cell(X_current, Y, R)) &
    succ(X_prev, X_current) &
    path(X_goal, Y, X_prev, Y)

% The defender can move the king to all squares (including the throne) vertically.
legal_move(defender, move(X, Y_current, X, Y_goal)) :-
    true(control(defender)) &
    true(cell(X, Y_current, king)) &
    succ(Y_current, Y_next) &
    path(X, Y_next, X, Y_goal)

legal_move(defender, move(X, Y_current, X, Y_goal)) :-
    true(control(defender)) &
    true(cell(X, Y_current, king)) &
    succ(Y_prev, Y_current) &
    path(X, Y_goal, X, Y_prev)

% The defender can move the king to all squares (including the throne) horizontally.
legal_move(defender, move(X_current, Y, X_goal, Y)) :-
    true(control(defender)) &
    true(cell(X_current, Y, king)) &
    succ(X_current, X_next) &
    path(X_next, Y, X_goal, Y)

legal_move(defender, move(X_current, Y, X_goal, Y)) :-
    true(control(defender)) &
    true(cell(X_current, Y, king)) &
    succ(X_prev, X_current) &
    path(X_goal, Y, X_prev, Y)

% Players must perform one legal move on their own turn.
% If they can't, they lose the game.
legal(R, move(X1, Y1, X2, Y2)) :-
    true(control(R)) &
    legal_move(R, move(X1, Y1, X2, Y2))

% Do nothing on the other player's turn.
legal(attacker, noop) :- true(control(defender))
legal(defender, noop) :- true(control(attacker))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%  UPDATE RULES  %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Alternate turns between players.
next(control(defender)) :- true(control(attacker))
next(control(attacker)) :- true(control(defender))

% Moving a piece leaves its previous cell blank.
next(cell(X_current, Y_current, blank)) :-
    does(_, move(X_current, Y_current, _, _))

% Moving a piece updates the value of the destination square.
next(cell(X_goal, Y_goal, V)) :-
    true(cell(X_current, Y_current, V)) &
    does(_, move(X_current, Y_current, X_goal, Y_goal))

% Capturing a piece leaves its cell blank.
next(cell(X, Y, blank)) :- captured(X, Y)

% All other cells remain the same in the next state.
next(cell(X, Y, V)) :-
    true(cell(X, Y, V)) &
    ~updated(X, Y)

% The king has been captured, the next state is terminal.
next(king_captured) :-
    captured(X, Y) &
    true(cell(X, Y, king))

third_repetition :-
    current_state(
        X1Y1, X1Y2, X1Y3, X1Y4, X1Y5,
        X2Y1, X2Y2, X2Y3, X2Y4, X2Y5,
        X3Y1, X3Y2, X3Y3, X3Y4, X3Y5,
        X4Y1, X4Y2, X4Y3, X4Y4, X4Y5,
        X5Y1, X5Y2, X5Y3, X5Y4, X5Y5) &
    true(state_count(2,
        X1Y1, X1Y2, X1Y3, X1Y4, X1Y5,
        X2Y1, X2Y2, X2Y3, X2Y4, X2Y5,
        X3Y1, X3Y2, X3Y3, X3Y4, X3Y5,
        X4Y1, X4Y2, X4Y3, X4Y4, X4Y5,
        X5Y1, X5Y2, X5Y3, X5Y4, X5Y5))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%  TERMINAL STATES  %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The game ends if any player has been defeated.
terminal :- attacker_wins
terminal :- defender_wins

% The game has 6 types of terminal states:
% 1. The king escapes to one of the corner squares.
% 2. The king is on the throne and surrounded by 4 attackers,
%       or next to the throne and surrounded by 3 attackers and the throne.
%       Next to the throne is one of: (6,7) (7,6) (8,7) (7,8).
% 3. The king is captured by 2 attackers on any other square.
% 4. The king and all defenders are surrounded by an unbroken ring of attackers.
% 		We ignore this rule for now, as it is hard to implement and very likely
% 		to lead to another win condition for the attacker anyway.
% 5. Either player has no legal moves left.
% 6. One of the players causes a perpetual repetition: if a board position is
%		repeated for the 3rd time, the player who had the last move loses.

% (1) The king moves to one of the edge squares.
defender_wins :-
    true(cell(X, Y, king)) &
    edge(X, Y)

% (3) The king is captured.
attacker_wins :- true(king_captured)

% (4) The king and all defenders are surrounded by attackers.
%       -> Not implemented.

% (5) If a player has no legal moves, they lose the game.
attacker_wins :-
    true(control(defender)) &
    ~any_move

defender_wins :-
    true(control(attacker)) &
    ~any_move

% (6) If the same board position is repeated for the third time, the game ends.
attacker_wins :-
    third_repetition &
    true(control(attacker))

defender_wins :-
    third_repetition &
    true(control(defender))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%  GOALS & UTILITY  %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The winner gets 100 utility, the loser gets 0.
goal(attacker, 100) :- attacker_wins
goal(defender,   0) :- attacker_wins

goal(attacker,   0) :- defender_wins
goal(defender, 100) :- defender_wins

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%  SUPPORTING CONCEPTS  %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The board is 13x13 squares.
index(1)
index(2)
index(3)
index(4)
index(5)

% The center square is the throne.
throne(3, 3)

% Squares with x = 1, x = 13, y = 1, or y = 13 are edge squares.
edge(1, Y) :- index(Y)
edge(5, Y) :- index(Y)
edge(X, 1) :- index(X)
edge(X, 5) :- index(X)

% Cells can be occupied by an attacker, a defender, the king, or it can be blank.
value(attacker)
value(defender)
value(king)
value(blank)

% Cells are hostile if they can participate in the capture of an opposing piece.
hostile(attacker, X, Y) :- true(cell(X, Y, king))
hostile(attacker, X, Y) :- true(cell(X, Y, defender))
hostile(defender, X, Y) :- true(cell(X, Y, attacker))

% The throne is always hostile to attackers.
hostile(attacker, 3, 3)

% The throne is hostile to defenders only if it is unoccupied.
hostile(defender, 3, 3) :- true(cell(3, 3, blank))

% The king cannot be captured on or next to the throne.
king_capturable(X, Y) :-
    index(X) &
    index(Y) &
    ~throne(X, Y) &
    ~next_to_throne(X, Y)

% A horizontal or vertical path (base case).
% Every blank cell has a path to itself.
path(X, Y, X, Y) :- true(cell(X, Y, blank))

% Vertical path (recursive case).
path(X, Y_current, X, Y_goal) :-
    true(cell(X, Y_current, blank)) &
    distinct(Y_current, Y_goal) &
    succ(Y_current, Y_next) &
    path(X, Y_next, X, Y_goal)

% Horizontal path (recursive case).
path(X_current, Y, X_goal, Y) :-
    true(cell(X_current, Y, blank)) &
    distinct(X_current, X_goal) &
    succ(X_current, X_next) &
    path(X_next, Y, X_goal, Y)

% Is there any legal move for the player whose turn it is?
any_move :-
    true(control(R)) &
    legal_move(R, move(_, _, _, _))

% Capturing regular pieces vertically.
captured(X, Y2) :-
    % The opponent moved a piece next to the captured piece.
    does(R1, move(_, _, X, Y1)) &
    % The three cells are next to each other.
    succ(Y1, Y2) &
    succ(Y2, Y3) &
    % The middle (captured) cell contains a different colored piece than the cells to its sides.
    true(cell(X, Y2, R2)) &
    distinct(R1, R2) &
    % The cell to the other side than the moved piece is also hostile to the middle cell.
    hostile(R2, X, Y3)
    
captured(X, Y2) :-
    % The opponent moved a piece next to the captured piece.
    does(R1, move(_, _, X, Y1)) &
    % The three cells are next to each other.
    succ(Y3, Y2) &
    succ(Y2, Y1) &
    % The middle (captured) cell contains a different colored piece than the cells to its side.
    true(cell(X, Y2, R2)) &
    distinct(R1, R2) &
    % The cell to the other side than the moved piece is also hostile to the middle cell.
    hostile(R2, X, Y3)

% Capturing regular pieces horizontically.
captured(X2, Y) :-
    % The opponent moved a piece next to the captured piece.
    does(R1, move(_, _, X1, Y)) &
    % The three cells are next to each other.
    succ(X1, X2) &
    succ(X2, X3) &
    % The middle (captured) cell contains a different colored piece than the cells to its side.
    true(cell(X2, Y, R2)) &
    distinct(R1, R2) &
    % The cell to the other side than the moved piece is also hostile to the middle cell.
    hostile(R2, X3, Y)

captured(X2, Y) :-
    % The opponent moved a piece next to the captured piece.
    does(R1, move(_, _, X1, Y)) &
    % The three cells are next to each other.
    succ(X3, X2) &
    succ(X2, X1) &
    % The middle (captured) cell contains a different colored piece than the cells to its side.
    true(cell(X2, Y, R2)) &
    distinct(R1, R2) &
    % The cell to the other side than the moved piece is also hostile to the middle cell.
    hostile(R2, X3, Y)

% Capturing the king vertically.
captured(X, Y2) :-
    % The opponent moved a piece next to the captured piece.
    does(attacker, move(_, _, X, Y1)) &
    % The three cells are next to each other.
    succ(Y1, Y2) &
    succ(Y2, Y3) &
    % The middle (captured) cell contains a different colored piece than the cells to its sides.
    true(cell(X, Y2, king)) &
    % The cell to the other side than the moved piece is also hostile to the middle cell.
    hostile(defender, X, Y3) &
    king_capturable(X, Y2)

captured(X, Y2) :-
    % The opponent moved a piece next to the captured piece.
    does(attacker, move(_, _, X, Y1)) &
    % The three cells are next to each other.
    succ(Y3, Y2) &
    succ(Y2, Y1) &
    % The middle (captured) cell contains a different colored piece than the cells to its side.
    true(cell(X, Y2, king)) &
    % The cell to the other side than the moved piece is also hostile to the middle cell.
    hostile(defender, X, Y3) &
    king_capturable(X, Y2)

% Capturing the king horizontically.
captured(X2, Y) :-
    % The opponent moved a piece next to the captured piece.
    does(attacker, move(_, _, X1, Y)) &
    % The three cells are next to each other.
    succ(X1, X2) &
    succ(X2, X3) &
    % The middle (captured) cell contains a different colored piece than the cells to its side.
    true(cell(X2, Y, king)) &
    % The cell to the other side than the moved piece is also hostile to the middle cell.
    hostile(defender, X3, Y) &
    king_capturable(X2, Y)

captured(X2, Y) :-
    % The opponent moved a piece next to the captured piece.
    does(attacker, move(_, _, X1, Y)) &
    % The three cells are next to each other.
    succ(X3, X2) &
    succ(X2, X1) &
    % The middle (captured) cell contains a different colored piece than the cells to its side.
    true(cell(X2, Y, king)) &
    % The cell to the other side than the moved piece is also hostile to the middle cell.
    hostile(defender, X3, Y) &
    king_capturable(X2, Y)

% If a player moves a piece, the source and destination squares are updated.
updated(X, Y) :- 
    true(control(R)) &
    does(R, move(X, Y,  _, _))

updated(X, Y) :- 
    true(control(R)) &
    does(R, move(_, _, X, Y))

% If a piece has been captured, its square gets updated this turn.
updated(X, Y) :- captured(X, Y)

% A state can occur at most twice during a single game.
% If any state occurs a third time, the game ends.
valid_state_count(1)
valid_state_count(2)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%  STATE HISTORY  %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set the state counter to 1 if this is a new state.
next(state_count(1,
    X1Y1, X1Y2, X1Y3, X1Y4, X1Y5,
    X2Y1, X2Y2, X2Y3, X2Y4, X2Y5,
    X3Y1, X3Y2, X3Y3, X3Y4, X3Y5,
    X4Y1, X4Y2, X4Y3, X4Y4, X4Y5,
    X5Y1, X5Y2, X5Y3, X5Y4, X5Y5)) :-
    current_state(
        X1Y1, X1Y2, X1Y3, X1Y4, X1Y5,
        X2Y1, X2Y2, X2Y3, X2Y4, X2Y5,
        X3Y1, X3Y2, X3Y3, X3Y4, X3Y5,
        X4Y1, X4Y2, X4Y3, X4Y4, X4Y5,
        X5Y1, X5Y2, X5Y3, X5Y4, X5Y5) &
    valid_state_count(N) &
    ~true(state_count(N,
        X1Y1, X1Y2, X1Y3, X1Y4, X1Y5,
        X2Y1, X2Y2, X2Y3, X2Y4, X2Y5,
        X3Y1, X3Y2, X3Y3, X3Y4, X3Y5,
        X4Y1, X4Y2, X4Y3, X4Y4, X4Y5,
        X5Y1, X5Y2, X5Y3, X5Y4, X5Y5))

% Increment the state counter for states that have occurred before.
next(state_count(2,
    X1Y1, X1Y2, X1Y3, X1Y4, X1Y5,
    X2Y1, X2Y2, X2Y3, X2Y4, X2Y5,
    X3Y1, X3Y2, X3Y3, X3Y4, X3Y5,
    X4Y1, X4Y2, X4Y3, X4Y4, X4Y5,
    X5Y1, X5Y2, X5Y3, X5Y4, X5Y5)) :-
    current_state(
        X1Y1, X1Y2, X1Y3, X1Y4, X1Y5,
        X2Y1, X2Y2, X2Y3, X2Y4, X2Y5,
        X3Y1, X3Y2, X3Y3, X3Y4, X3Y5,
        X4Y1, X4Y2, X4Y3, X4Y4, X4Y5,
        X5Y1, X5Y2, X5Y3, X5Y4, X5Y5) &
    true(state_count(1,
        X1Y1, X1Y2, X1Y3, X1Y4, X1Y5,
        X2Y1, X2Y2, X2Y3, X2Y4, X2Y5,
        X3Y1, X3Y2, X3Y3, X3Y4, X3Y5,
        X4Y1, X4Y2, X4Y3, X4Y4, X4Y5,
        X5Y1, X5Y2, X5Y3, X5Y4, X5Y5))

% Remember all unchanged state counts for previous states.
next(state_count(N,
    X1Y1, X1Y2, X1Y3, X1Y4, X1Y5,
    X2Y1, X2Y2, X2Y3, X2Y4, X2Y5,
    X3Y1, X3Y2, X3Y3, X3Y4, X3Y5,
    X4Y1, X4Y2, X4Y3, X4Y4, X4Y5,
    X5Y1, X5Y2, X5Y3, X5Y4, X5Y5)) :-
    valid_state_count(N) &
    ~current_state(
        X1Y1, X1Y2, X1Y3, X1Y4, X1Y5,
        X2Y1, X2Y2, X2Y3, X2Y4, X2Y5,
        X3Y1, X3Y2, X3Y3, X3Y4, X3Y5,
        X4Y1, X4Y2, X4Y3, X4Y4, X4Y5,
        X5Y1, X5Y2, X5Y3, X5Y4, X5Y5) &
    true(state_count(N,
        X1Y1, X1Y2, X1Y3, X1Y4, X1Y5,
        X2Y1, X2Y2, X2Y3, X2Y4, X2Y5,
        X3Y1, X3Y2, X3Y3, X3Y4, X3Y5,
        X4Y1, X4Y2, X4Y3, X4Y4, X4Y5,
        X5Y1, X5Y2, X5Y3, X5Y4, X5Y5))

% Collect the current state in one predicate.
current_state(
    X1Y1, X1Y2, X1Y3, X1Y4, X1Y5,
    X2Y1, X2Y2, X2Y3, X2Y4, X2Y5,
    X3Y1, X3Y2, X3Y3, X3Y4, X3Y5,
    X4Y1, X4Y2, X4Y3, X4Y4, X4Y5,
    X5Y1, X5Y2, X5Y3, X5Y4, X5Y5) :-
    true(cell(1, 1,   X1Y1))   &
    true(cell(1, 2,   X1Y2))   &
    true(cell(1, 3,   X1Y3))   &
    true(cell(1, 4,   X1Y4))   &
    true(cell(1, 5,   X1Y5))   &
    true(cell(2, 1,   X2Y1))   &
    true(cell(2, 2,   X2Y2))   &
    true(cell(2, 3,   X2Y3))   &
    true(cell(2, 4,   X2Y4))   &
    true(cell(2, 5,   X2Y5))   &
    true(cell(3, 1,   X3Y1))   &
    true(cell(3, 2,   X3Y2))   &
    true(cell(3, 3,   X3Y3))   &
    true(cell(3, 4,   X3Y4))   &
    true(cell(3, 5,   X3Y5))   &
    true(cell(4, 1,   X4Y1))   &
    true(cell(4, 2,   X4Y2))   &
    true(cell(4, 3,   X4Y3))   &
    true(cell(4, 4,   X4Y4))   &
    true(cell(4, 5,   X4Y5))   &
    true(cell(5, 1,   X5Y1))   &
    true(cell(5, 2,   X5Y2))   &
    true(cell(5, 3,   X5Y3))   &
    true(cell(5, 4,   X5Y4))   &
    true(cell(5, 5,   X5Y5))   &

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%  SUCCESSOR RELATION  %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% GDL has no built-in counting support, so an
% explicitly stated successor relation is used instead.
succ(0, 1)
succ(1, 2)
succ(2, 3)
succ(3, 4)
succ(4, 5)

% ------------------- TURN TIMER for debugging ---------------- %

% Turns have numbers in the range 1-500.
base(turn(N)) :- succ(_, N)

% The turn timer starts at 1.
init(turn(1))

% Increment the turn timer.
next(turn(N_next)) :-
    true(turn(N_current)) &
    succ(N_current, N_next)


succ(5, 6)
succ(6, 7)
succ(7, 8)
succ(8, 9)
succ(9, 10)
succ(10, 11)
succ(11, 12)
succ(12, 13)
succ(13, 14)
succ(14, 15)
succ(15, 16)
succ(16, 17)
succ(17, 18)
succ(18, 19)
succ(19, 20)
succ(20, 21)
succ(21, 22)
succ(22, 23)
succ(23, 24)
succ(24, 25)
succ(25, 26)
succ(26, 27)
succ(27, 28)
succ(28, 29)
succ(29, 30)
succ(30, 31)
succ(31, 32)
succ(32, 33)
succ(33, 34)
succ(34, 35)
succ(35, 36)
succ(36, 37)
succ(37, 38)
succ(38, 39)
succ(39, 40)
succ(40, 41)
succ(41, 42)
succ(42, 43)
succ(43, 44)
succ(44, 45)
succ(45, 46)
succ(46, 47)
succ(47, 48)
succ(48, 49)
succ(49, 50)
succ(50, 51)
succ(51, 52)
succ(52, 53)
succ(53, 54)
succ(54, 55)
succ(55, 56)
succ(56, 57)
succ(57, 58)
succ(58, 59)
succ(59, 60)
succ(60, 61)
succ(61, 62)
succ(62, 63)
succ(63, 64)
succ(64, 65)
succ(65, 66)
succ(66, 67)
succ(67, 68)
succ(68, 69)
succ(69, 70)
succ(70, 71)
succ(71, 72)
succ(72, 73)
succ(73, 74)
succ(74, 75)
succ(75, 76)
succ(76, 77)
succ(77, 78)
succ(78, 79)
succ(79, 80)
succ(80, 81)
succ(81, 82)
succ(82, 83)
succ(83, 84)
succ(84, 85)
succ(85, 86)
succ(86, 87)
succ(87, 88)
succ(88, 89)
succ(89, 90)
succ(90, 91)
succ(91, 92)
succ(92, 93)
succ(93, 94)
succ(94, 95)
succ(95, 96)
succ(96, 97)
succ(97, 98)
succ(98, 99)
succ(99, 100)
succ(100, 101)
succ(101, 102)
succ(102, 103)
succ(103, 104)
succ(104, 105)
succ(105, 106)
succ(106, 107)
succ(107, 108)
succ(108, 109)
succ(109, 110)
succ(110, 111)
succ(111, 112)
succ(112, 113)
succ(113, 114)
succ(114, 115)
succ(115, 116)
succ(116, 117)
succ(117, 118)
succ(118, 119)
succ(119, 120)
succ(120, 121)
succ(121, 122)
succ(122, 123)
succ(123, 124)
succ(124, 125)
succ(125, 126)
succ(126, 127)
succ(127, 128)
succ(128, 129)
succ(129, 130)
succ(130, 131)
succ(131, 132)
succ(132, 133)
succ(133, 134)
succ(134, 135)
succ(135, 136)
succ(136, 137)
succ(137, 138)
succ(138, 139)
succ(139, 140)
succ(140, 141)
succ(141, 142)
succ(142, 143)
succ(143, 144)
succ(144, 145)
succ(145, 146)
succ(146, 147)
succ(147, 148)
succ(148, 149)
succ(149, 150)
succ(150, 151)
succ(151, 152)
succ(152, 153)
succ(153, 154)
succ(154, 155)
succ(155, 156)
succ(156, 157)
succ(157, 158)
succ(158, 159)
succ(159, 160)
succ(160, 161)
succ(161, 162)
succ(162, 163)
succ(163, 164)
succ(164, 165)
succ(165, 166)
succ(166, 167)
succ(167, 168)
succ(168, 169)
succ(169, 170)
succ(170, 171)
succ(171, 172)
succ(172, 173)
succ(173, 174)
succ(174, 175)
succ(175, 176)
succ(176, 177)
succ(177, 178)
succ(178, 179)
succ(179, 180)
succ(180, 181)
succ(181, 182)
succ(182, 183)
succ(183, 184)
succ(184, 185)
succ(185, 186)
succ(186, 187)
succ(187, 188)
succ(188, 189)
succ(189, 190)
succ(190, 191)
succ(191, 192)
succ(192, 193)
succ(193, 194)
succ(194, 195)
succ(195, 196)
succ(196, 197)
succ(197, 198)
succ(198, 199)
succ(199, 200)
succ(200, 201)
succ(201, 202)
succ(202, 203)
succ(203, 204)
succ(204, 205)
succ(205, 206)
succ(206, 207)
succ(207, 208)
succ(208, 209)
succ(209, 210)
succ(210, 211)
succ(211, 212)
succ(212, 213)
succ(213, 214)
succ(214, 215)
succ(215, 216)
succ(216, 217)
succ(217, 218)
succ(218, 219)
succ(219, 220)
succ(220, 221)
succ(221, 222)
succ(222, 223)
succ(223, 224)
succ(224, 225)
succ(225, 226)
succ(226, 227)
succ(227, 228)
succ(228, 229)
succ(229, 230)
succ(230, 231)
succ(231, 232)
succ(232, 233)
succ(233, 234)
succ(234, 235)
succ(235, 236)
succ(236, 237)
succ(237, 238)
succ(238, 239)
succ(239, 240)
succ(240, 241)
succ(241, 242)
succ(242, 243)
succ(243, 244)
succ(244, 245)
succ(245, 246)
succ(246, 247)
succ(247, 248)
succ(248, 249)
succ(249, 250)
succ(250, 251)
succ(251, 252)
succ(252, 253)
succ(253, 254)
succ(254, 255)
succ(255, 256)
succ(256, 257)
succ(257, 258)
succ(258, 259)
succ(259, 260)
succ(260, 261)
succ(261, 262)
succ(262, 263)
succ(263, 264)
succ(264, 265)
succ(265, 266)
succ(266, 267)
succ(267, 268)
succ(268, 269)
succ(269, 270)
succ(270, 271)
succ(271, 272)
succ(272, 273)
succ(273, 274)
succ(274, 275)
succ(275, 276)
succ(276, 277)
succ(277, 278)
succ(278, 279)
succ(279, 280)
succ(280, 281)
succ(281, 282)
succ(282, 283)
succ(283, 284)
succ(284, 285)
succ(285, 286)
succ(286, 287)
succ(287, 288)
succ(288, 289)
succ(289, 290)
succ(290, 291)
succ(291, 292)
succ(292, 293)
succ(293, 294)
succ(294, 295)
succ(295, 296)
succ(296, 297)
succ(297, 298)
succ(298, 299)
succ(299, 300)
succ(300, 301)
succ(301, 302)
succ(302, 303)
succ(303, 304)
succ(304, 305)
succ(305, 306)
succ(306, 307)
succ(307, 308)
succ(308, 309)
succ(309, 310)
succ(310, 311)
succ(311, 312)
succ(312, 313)
succ(313, 314)
succ(314, 315)
succ(315, 316)
succ(316, 317)
succ(317, 318)
succ(318, 319)
succ(319, 320)
succ(320, 321)
succ(321, 322)
succ(322, 323)
succ(323, 324)
succ(324, 325)
succ(325, 326)
succ(326, 327)
succ(327, 328)
succ(328, 329)
succ(329, 330)
succ(330, 331)
succ(331, 332)
succ(332, 333)
succ(333, 334)
succ(334, 335)
succ(335, 336)
succ(336, 337)
succ(337, 338)
succ(338, 339)
succ(339, 340)
succ(340, 341)
succ(341, 342)
succ(342, 343)
succ(343, 344)
succ(344, 345)
succ(345, 346)
succ(346, 347)
succ(347, 348)
succ(348, 349)
succ(349, 350)
succ(350, 351)
succ(351, 352)
succ(352, 353)
succ(353, 354)
succ(354, 355)
succ(355, 356)
succ(356, 357)
succ(357, 358)
succ(358, 359)
succ(359, 360)
succ(360, 361)
succ(361, 362)
succ(362, 363)
succ(363, 364)
succ(364, 365)
succ(365, 366)
succ(366, 367)
succ(367, 368)
succ(368, 369)
succ(369, 370)
succ(370, 371)
succ(371, 372)
succ(372, 373)
succ(373, 374)
succ(374, 375)
succ(375, 376)
succ(376, 377)
succ(377, 378)
succ(378, 379)
succ(379, 380)
succ(380, 381)
succ(381, 382)
succ(382, 383)
succ(383, 384)
succ(384, 385)
succ(385, 386)
succ(386, 387)
succ(387, 388)
succ(388, 389)
succ(389, 390)
succ(390, 391)
succ(391, 392)
succ(392, 393)
succ(393, 394)
succ(394, 395)
succ(395, 396)
succ(396, 397)
succ(397, 398)
succ(398, 399)
succ(399, 400)
succ(400, 401)
succ(401, 402)
succ(402, 403)
succ(403, 404)
succ(404, 405)
succ(405, 406)
succ(406, 407)
succ(407, 408)
succ(408, 409)
succ(409, 410)
succ(410, 411)
succ(411, 412)
succ(412, 413)
succ(413, 414)
succ(414, 415)
succ(415, 416)
succ(416, 417)
succ(417, 418)
succ(418, 419)
succ(419, 420)
succ(420, 421)
succ(421, 422)
succ(422, 423)
succ(423, 424)
succ(424, 425)
succ(425, 426)
succ(426, 427)
succ(427, 428)
succ(428, 429)
succ(429, 430)
succ(430, 431)
succ(431, 432)
succ(432, 433)
succ(433, 434)
succ(434, 435)
succ(435, 436)
succ(436, 437)
succ(437, 438)
succ(438, 439)
succ(439, 440)
succ(440, 441)
succ(441, 442)
succ(442, 443)
succ(443, 444)
succ(444, 445)
succ(445, 446)
succ(446, 447)
succ(447, 448)
succ(448, 449)
succ(449, 450)
succ(450, 451)
succ(451, 452)
succ(452, 453)
succ(453, 454)
succ(454, 455)
succ(455, 456)
succ(456, 457)
succ(457, 458)
succ(458, 459)
succ(459, 460)
succ(460, 461)
succ(461, 462)
succ(462, 463)
succ(463, 464)
succ(464, 465)
succ(465, 466)
succ(466, 467)
succ(467, 468)
succ(468, 469)
succ(469, 470)
succ(470, 471)
succ(471, 472)
succ(472, 473)
succ(473, 474)
succ(474, 475)
succ(475, 476)
succ(476, 477)
succ(477, 478)
succ(478, 479)
succ(479, 480)
succ(480, 481)
succ(481, 482)
succ(482, 483)
succ(483, 484)
succ(484, 485)
succ(485, 486)
succ(486, 487)
succ(487, 488)
succ(488, 489)
succ(489, 490)
succ(490, 491)
succ(491, 492)
succ(492, 493)
succ(493, 494)
succ(494, 495)
succ(495, 496)
succ(496, 497)
succ(497, 498)
succ(498, 499)
succ(499, 500)